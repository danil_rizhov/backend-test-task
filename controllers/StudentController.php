<?php

namespace app\Controllers;

class StudentController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreateStudent()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $student = new Student();
        $student->scenario = Student:: SCENARIO_CREATE;
        $student->attributes = \yii::$app->request->post();

        if($student->validate())
        {
            $student->save();
            return array('status' => true, 'data'=> 'Student record is successfully updated');
        }
        else
        {
            return array('status'=>false,'data'=>$student->getErrors());    
        }
    }
}